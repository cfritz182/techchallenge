#!/bin/sh

if [ $# -eq 0 ]
  then
    dot -Tpdf mindmap.dot > mindmap.dot.pdf
  else
    dot -Tpdf $1 > $1.pdf
fi
