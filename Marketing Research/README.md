Here is the directory for all Marketing Research Papers, Interviews and so on

* [E-Technik Chair which do research in HEAT USAGE AND DEMAND OF BUILDINGS](https://www.ewk.ei.tum.de/forschung/themen/ses/)
* [Chair of Building Technology and Climate Responsive Design Professor Thomas Auer](https://www.klima.ar.tum.de/en/start/)
* [German article about a mapping robot with code examples](https://m.heise.de/ct/artikel/An-der-naechsten-Ecke-links-290662.html)
* [Wikipedia Article about SLM](https://de.m.wikipedia.org/wiki/Simultaneous_Localization_and_Mapping)
* [IBM Robot Maps Data Center Temperature and Humidity](http://www.datacenterknowledge.com/archives/2013/05/30/ibm-robot-maps-temperature-and-humidity-in-the-data-center)
* [Academic Article about Robotic Mapping and Monitoring of Data Centers](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.643.6477&rep=rep1&type=pdf)
* [Product example for room-climate-controlling](https://www.housecontrollers.de/allgemein/cubesensors-kleiner-wuerfel-ueberwacht-das-raumklima/)
* [CO2 Sensor](http://www.komputer.de/zen/index.php?main_page=product_info&products_id=325)
* [Gesundheitliche Bewertung von Kohlendioxid in der Innenraumluft](https://www.umweltbundesamt.de/sites/default/files/medien/pdfs/kohlendioxid_2008.pdf)