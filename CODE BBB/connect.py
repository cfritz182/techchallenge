#!/usr/bin/python2.7
import os, sys, subprocess
import bluepy
from bluepy import sensortag
from bluepy.btle import UUID, Peripheral, DefaultDelegate, AssignedNumbers
import msp # local file msp.py
import math
import time
import argparse

if __name__=="__main__":
    ''' This code was important, before bluepy was used
    print("1)  looking for bluetooth-dongles, found this:")
    os.system("hcitool dev | tail -n 1")

    print("2a) scanning for Sensortag")
    #if "24:71:89:07:74:80 CC2650 SensorTag" in subprocess.check_output(['hcitool', 'lescan']):
        #print("    found.")

    print("3)  connecting")
    try:
        r = subprocess.check_output(['hcitool', 'lecc', '24:71:89:07:74:80'])
    except subprocess.CalledProcessError:
        exit()
    if r[:18] == "Connection handle ":
        handle = int(r[18:])
        print(handle)
        print("    successful")
    else:
        print("didn't get handle") # should never occur
    os.system("gatttool -b 24:71:89:07:74:80 -I")

    print("4)  reading")

    print("disconnecting...")
    #os.system("hcitool ledc %d" % handle)
    '''

    parser = argparse.ArgumentParser()
    parser.add_argument('tag', action='store', help='MAC of the SensorTag')
    parser.add_argument('msp', action='store', help='MAC of MSP432s Bluetooth LE module')
    parser.add_argument('-n', action='store', dest='count', default=0,
            type=int, help="Number of times to loop data (default: infinite)")
    parser.add_argument('-t',action='store',type=float, default=5.0, help='time between polling (default: 5s)')
    arg = parser.parse_args(sys.argv[1:])

    print('Connecting to ' + arg.tag)
    tag = sensortag.SensorTag(arg.tag)
    print('Connecting to ' + arg.msp)
    msp = msp.MSP(arg.msp)

    # Enabling position service
    msp.position.enable()

    # Enabling SensorTags Sensors
    tag.IRtemperature.enable()
    tag.humidity.enable()
    tag.barometer.enable()
    tag.accelerometer.enable()
    tag.magnetometer.enable()
    tag.gyroscope.enable()
    tag.keypress.enable()
    if tag.lightmeter is not None: # Check if the SensorTag has the lightsensor
        tag.lightmeter.enable()

    # Some sensors need some time for initialization.
    # Not waiting here after enabling a sensor, the first read value might be empty or incorrect.
    time.sleep(1.0)

    # reading in sensordata and MSPs position and writing it to data.csv
    counter=1
    with open('data.csv','wb') as file:
        counter=1
        while True:
            pos = msp.position.read()
            tmp = tag.IRtemperature.read()
            hum = tag.humidity.read()
            bar = tag.barometer.read()
            acc = tag.accelerometer.read()
            mag = tag.magnetometer.read()
            gyr = tag.gyroscope.read()

            # printing important data
            print "Position: ", pos
            print 'Temperature: ', tmp[0]
            #print "Humidity: ", hum
            #print "Barometer: ", bar
            #print "Accelerometer: ", acc
            #print "Magnetometer: ", mag
            #print "Gyroscope: ", gyr
            #if tag.lightmeter is not None:
            lig = tag.lightmeter.read()
            print "Light: ", lig 

            file.write(time.time()) # time stamp
            file.write(',')
            file.write(pos) # position
            file.write(',')
            file.write(str(counter))
            file.write(',')
            file.write(str(tmp[0]))
            file.write(',')
            file.write(str(tmp[1]))
            file.write(',')
            file.write(str(lig))
            file.write('\n')

            if counter >= arg.count and arg.count != 0:
                break
            counter += 1

    tag.disconnect()
    del tag
    del msp
