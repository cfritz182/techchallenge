#!/usr/bin/python2.7
from bluepy.btle import UUID, Peripheral, DefaultDelegate, AssignedNumbers
import struct
import math

def _TI_UUID(val):
    return UUID("%08X-0000-1000-8000-00805F9B34FB" % (0x00000000+val))

AUTODETECT = "-"
msp_name = "Ludwig BLE"

class MSPPosition:
    xdataUUID = _TI_UUID(0xFFF1)
    ydataUUID = _TI_UUID(0xFFF2)

    def __init__(self, periph):
        self.periph = periph
        self.xdata = None
        self.ydata = None

    def enable(self):
        if self.xdata is None:
            self.xdata = self.service.getCharacteristics(self.dataUUID) [0]
        if self.ydata is None:
            self.ydata = self.service.getCharacteristics(self.dataUUID) [0]

    def read(self):
        '''Returns value in lux'''
        raw = struct.unpack('<h', self.data.read()) [0]
        m = raw & 0xFFF;
        e = (raw & 0xF000) >> 12;
        return 0.01 * (m << e)

    ''' not needed
    def disable(self):
        if self.ctrl is not None:
            self.ctrl.write(self.sensorOff)
    '''

class MSP(Peripheral):
    def __init__(self,addr,version=AUTODETECT):
        Peripheral.__init__(self,addr)
        if version==AUTODETECT:
            svcs = self.discoverServices()
            # Code to check if it is the MSP
            if _TI_UUID(0xAA70) in svcs:
                version = msp_name

        fwVers = self.getCharacteristics(uuid=AssignedNumbers.firmwareRevisionString)
        if len(fwVers) >= 1:
            self.firmwareVersion = fwVers[0].read().decode("utf-8")
        else:
            self.firmwareVersion = u''

        print(firmwareVersion)
        print(version)

        self.position = MSPPosition(self)

    ''' isn't needed
    def handleNotification(self, hnd, data):
        # NB: only one source of notifications at present
        # so we can ignore 'hnd'.
        val = struct.unpack("B", data)[0]
        down = (val & ~self.lastVal) & self.ALL_BUTTONS
        if down != 0:
            self.onButtonDown(down)
        up = (~val & self.lastVal) & self.ALL_BUTTONS
        if up != 0:
            self.onButtonUp(up)
        self.lastVal = val
        '''

if __name__ == "__main__":
    import time
    import sys
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('host', action='store',help='MAC of BT device')
    parser.add_argument('-n', action='store', dest='count', default=0,
            type=int, help="Number of times to loop data")
    parser.add_argument('-t',action='store',type=float, default=5.0, help='time between polling')
    #parser.add_argument('-A','--accelerometer', action='store_true',
            #default=False)
    #parser.add_argument('-P','--battery', action='store_true', default=False)

    arg = parser.parse_args(sys.argv[1:])

    print('Connecting to ' + arg.host)
    msp = MSP(arg.host)

    # Enabling position service
    msp.position.enable()

    time.sleep(1.0)

    counter=1
    while True:
       print("Position: ", msp.position.read())
       if counter >= arg.count and arg.count != 0:
           break
       counter += 1
       msp.waitForNotifications(arg.t)

    del msp
