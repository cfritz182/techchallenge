#   Code for the BBB

There are two python scripts.
* connect.py
* msp.py

to log the data into the data.csv file, hit
```shell
./connect.py <TAG-MAC> <MSP-MAC>
```
with <TAG-MAC> and <MSP-MAC> being the MACs of the SensorTag and MSP432s Bluetooth boosterpack and both files in the same directory.
