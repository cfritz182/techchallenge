\contentsline {section}{Literatur}{1}{Doc-Start}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Package information}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}MSP-EXP432P401R SimpleLink™ MSP432P401R LaunchPad}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}BOOSTXL-CC2650MA BoosterPack}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}BEAGLEBK BeagleBone Black Development Board}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Pololu Romi Chassi}{7}{section.2.4}
\contentsline {section}{\numberline {2.5}Joystick}{9}{section.2.5}
\contentsline {chapter}{\numberline {3}Theory}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Pulse-Width-Modulation}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Digital Analog Converter}{11}{section.3.2}
\contentsline {section}{\numberline {3.3}GPIO interrupts}{12}{section.3.3}
\contentsline {section}{\numberline {3.4}Bluetooth Low Energy}{12}{section.3.4}
\contentsline {section}{\numberline {3.5}Real Time Operating Systems - RTOS}{12}{section.3.5}
\contentsline {chapter}{\numberline {4}Assembly of the Robot}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Kits}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}Partlist}{17}{section.4.2}
\contentsline {section}{\numberline {4.3}Assembly of the main board}{18}{section.4.3}
\contentsline {chapter}{\numberline {5}Bluetooth-Network}{26}{chapter.5}
\contentsline {section}{\numberline {5.1}Goal}{26}{section.5.1}
\contentsline {section}{\numberline {5.2}Setting up the Beaglebone}{27}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Flashing the latest firmware}{27}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Accessing the Beaglebone via ssh}{27}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Basic Terminal commands}{27}{subsection.5.2.3}
\contentsline {subsubsection}{cd,ls}{27}{section*.2}
\contentsline {subsubsection}{sudo}{27}{section*.3}
\contentsline {subsection}{\numberline {5.2.4}Internet connection with the Beaglebone}{28}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Downloading and installing required packages}{29}{subsection.5.2.5}
\contentsline {section}{\numberline {5.3}Bluetooth Experiments}{30}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Check functionality with hci- and gatttool}{30}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}python-scripting}{30}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Writing the python code}{30}{section.5.4}
\contentsline {section}{\numberline {5.5}Visualising the data}{30}{section.5.5}
\contentsline {chapter}{\numberline {6}Troubleshooting}{31}{chapter.6}
\contentsline {section}{\numberline {6.1}ssh doesn't work}{31}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}ssh installed?}{31}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Cable correctly connected?}{31}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}no dhcp connection?}{31}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Beaglebone: no LAN-connection possible}{32}{section.6.2}
\contentsline {section}{\numberline {6.3}sudo won't let me execute commands}{32}{section.6.3}
\contentsline {section}{\numberline {6.4}git clone throws an error}{33}{section.6.4}
\contentsline {chapter}{\numberline {7}Code}{34}{chapter.7}
\contentsline {section}{\numberline {7.1}MSP432}{34}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}adc.c}{34}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}adc.h}{38}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Board.h}{38}{subsection.7.1.3}
\contentsline {subsection}{\numberline {7.1.4}hall.c}{42}{subsection.7.1.4}
\contentsline {subsection}{\numberline {7.1.5}hall.h}{45}{subsection.7.1.5}
\contentsline {subsection}{\numberline {7.1.6}main-tirtos.c}{45}{subsection.7.1.6}
\contentsline {subsection}{\numberline {7.1.7}MSP-EXP432P401R.c}{47}{subsection.7.1.7}
\contentsline {subsection}{\numberline {7.1.8}MSP-EXP432P401R.h}{66}{subsection.7.1.8}
\contentsline {subsection}{\numberline {7.1.9}MSP-EXP432P401R-TIRTOS.cmd}{71}{subsection.7.1.9}
\contentsline {subsection}{\numberline {7.1.10}position.c}{73}{subsection.7.1.10}
\contentsline {subsection}{\numberline {7.1.11}position.h}{76}{subsection.7.1.11}
\contentsline {subsection}{\numberline {7.1.12}pwm.c}{76}{subsection.7.1.12}
\contentsline {subsection}{\numberline {7.1.13}pwm.h}{80}{subsection.7.1.13}
\contentsline {subsection}{\numberline {7.1.14}simple-application-processor.c}{80}{subsection.7.1.14}
\contentsline {subsection}{\numberline {7.1.15}simple-application-processor.h}{101}{subsection.7.1.15}
\contentsline {subsection}{\numberline {7.1.16}simple-application-processor-params.c}{105}{subsection.7.1.16}
\contentsline {section}{\numberline {7.2}BeagleBoneBlack}{106}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}connect.py}{106}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}msp.c}{109}{subsection.7.2.2}
\contentsline {chapter}{\numberline {8}Further Readings}{114}{chapter.8}
\contentsline {chapter}{\numberline {9}Building instructions for the cupboard}{115}{chapter.9}
