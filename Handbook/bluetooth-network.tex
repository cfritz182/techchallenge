\chapter{Bluetooth-Network}

\section{Goal}
The goal of the project is to let the robot drive around autonomously and collecting the data.
To be able to visualize the data and to learn how to use a one-board-computer we will take a look at the beaglebone, how to set up an operating system and connect.

For this Chapter we will need:
\begin{itemize}
\item The Beaglebone Black
\item A bluetooth-low-energy dongle
\item The previously build robot
\item The TI SensorTag CC2650
\item A HDMI-micro to HDMI-cable
\item (optional) 5V-DC-power supply
\end{itemize}

This is how the connection should look like in the end: \\[1cm]
\begin{tikzpicture}
    \draw
    (0,5) node[circle,draw](msp) {Ludwig 1.}
    (0,3) node[circle,draw](st) {SensorTag}
    (6,4) node[circle,draw](bb) {Beaglebone}
    (6,0) node[draw](pc) {PC}
    (10,4) node[draw](sc) {screen}
    (10,2) node[rounded corners,draw,dashed](net) {internet};
    \draw[latex-latex] (msp) -- node[above]{bluetooth} (bb);
    \draw[latex-latex] (st) -- node[below]{bluetooth} (bb);
    \draw[-latex,dotted] (pc) -- node[right]{ssh} (bb);
    \draw[latex-latex,dotted] (pc) -- (net);
    \draw[latex-latex,dotted] (net) -- node[below]{lan} (bb);
    \draw[-latex] (bb) -- node[below]{hdmi} (sc);
\end{tikzpicture}

\section{Setting up the Beaglebone}
\subsection{Flashing the latest firmware}
{\it please notice: This step is not neccessary, but recommended by the manufacturer} \\
You can find all the latest operating system images and an explanation on this site:
https://beagleboard.org/latest-images

\subsection{Accessing the Beaglebone via ssh}
Plug together the Beaglebone and your PC with the USB-cable shipped in the Beaglebone box.

Next, type in
\begin{Verbatim}[frame=single]
ssh debian@192.168.7.1
\end{Verbatim}

The output should be something like (depending on the Beaglebone OS):

\begin{Verbatim}[frame=single]
Debian GNU/Linux 7

BeagleBoard.org Debian Image 2015-03-01

Support/FAQ: http://elinux.org/Beagleboard:BeagleBoneBlack_D

default username:password is [debian:temppwd]

debian@192.168.7.2's password:
\end{Verbatim}
and you are asked to input a password. As the answer to this question is written in the output, the default password should be "temppwd". Don't worry that no characters apear, when typing the password. This is only for security reasons.

Congratulations, you made it to access the Beaglebone!

\subsection{Basic Terminal commands}
\subsubsection{cd,ls}
\subsubsection{sudo}
{\tt sudo} is an abbreviation for "superuser do". The superuser is the administrator, who is able to modify everything.
Usual commands should never be executed as the superuser, because heavy damages could happen to the system. Some programs need to be executed because they are meant to modify the system, like installation programs. Other programs could accidentially delete important files for the system for example, what causes the system to be broken. So use this command only, if you are sure what you are doing. \\

{\bf How to use it:} \\
if you want to run a program (here called {\tt <program>} with superuser privileges you enter
\begin{Verbatim}[frame=single]
sudo <program>
\end{Verbatim}
as with ssh you will be asked to enter your password to authentificate yourself. After entering the program will be executed with superuser privileges.\\

{\bf Tipp:}\\
If you need to run many commands with sudo, you can simply enter
\begin{Verbatim}[frame=single]
sudo -s
\end{Verbatim}
to enter the root shell. Every now folling command will be executed with full permissions.
To exit this mode (the root shell), just enter {\tt exit}.

\subsection{Internet connection with the Beaglebone} 
There are many ways to set up an internet connection. The easiest is to use a LAN cable plugged into the Beaglebone. If the further described solution doesn't work, take a look at the troubleshooting pages, where a complete different way is explained.\\

Type in:
\begin{Verbatim}[frame=single]
ip link
\end{Verbatim}
The result might be like this:
\begin{Verbatim}[frame=single]
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state
    UNKNOWN mode DEFAULT
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc
    pfifo_fast state DOWN mode DEFAULT qlen 1000
    link/ether 78:a5:04:db:af:3a brd ff:ff:ff:ff:ff:ff
3: usb0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc
    pfifo_fast state UP mode DEFAULT qlen 1000
    link/ether de:5e:f4:ad:60:91 brd ff:ff:ff:ff:ff:ff
\end{Verbatim}
The output shows a list of all possible connections.
The first ({\tt lo}) is the loopback for testing.
The second ({\tt eth0}) is the Ethernet-port, where the LAN-cable should be plugged in.
The third ({\tt usb0}) is the USB-port. It is also possible to set up an internet connection on this port. How to do this is described in the troubleshooting chapter.\\

Now please plug in a LAN cable.\\

Mostly, the interface connects automaticly with the internet. With the command
\begin{Verbatim}[frame=single]
ping <address>
\end{Verbatim}
with {\tt <address>} being the adress of any website (like {\tt ti.com} for example) you can try out the connection.
If the Beaglebone is correctly connected, the output of this command is a never ending list, explaining, that it recieved a package. To stop this program, hit {\tt ctrl-C}. And proceed with the next section. \\

If not so and an error like {\tt Name or Service not known.} occurs, try out this:
\begin{Verbatim}[frame=single]
sudo dhclient eth0
\end{Verbatim}
the ethernet connection is enabled. Retest, if the connection works out by pinging like before. If it is still not working after a minute have a look at the troubleshooting pages.

\subsection{Downloading and installing required packages}
We will need the following software:
\begin{description}
\item[bluez] Kernel modules to be able to "talk the bluetooth language"
\item[bluez-utils] programs providing an interface to the last mentioned package
\item[git] version control system for tracking changes in computer files
\item[python] a programming language with the interpreter.
\end{description}

To download, install and remove packages there is the debian package manager, accessed by the program {\tt apt-get}.

Before the installation of the above packages it is recommended to upgrade the whole system with all programs. To do so, enter
\begin{Verbatim}[frame=single]
sudo apt-get update
\end{Verbatim}
and after that
\begin{Verbatim}[frame=single]
sudo apt-get upgrade
\end{Verbatim}
The whole process might take a few minutes.\\

And now we will install all needed packages with
\begin{Verbatim}[frame=single]
sudo apt-get install bluez bluez-utils git python
\end{Verbatim}

\section{Bluetooth Experiments}
\subsection{Check functionality with hci- and gatttool}
Before coding with python and getting unknown errors it is helpful to first try out, if the bluetooth adapter is able to connect and read out data from devices. We will do a quick test here with the SensorTag. Please make sure that the Bluetooth dongle is plugged in at the Beaglebone. \\

Test if the Bluetooth dongle is recognized:
\begin{Verbatim}[frame=single]
lsusb
\end{Verbatim}


\subsection{python-scripting}
Programming in python is pretty simple. Especcially, because there are packages for every purpose.
There is also a python package for the Bluetooth-Low-energy network that can be installed with
\begin{Verbatim}[frame=single]
git clone https://github.com/IanHarvey/bluepy.git --depth 1;
cd bluepy && python setup.py build && sudo python setup.py install
\end{Verbatim}

Now the connection can easily be set up. A documentation about all the python functions in this package can be found on 

\section{Writing the python code}


\section{Visualising the data}
