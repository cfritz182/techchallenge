/* EDUROBOTICA 2018
 * This file sets the Length and the angle in comparison and calculated the current position
 * The result is Set to the Characteristics 1 and 2 of the single application processor example
 *  position.c
 */
/*Standart Includes*/
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>
/*Enable Math options*/
#include <math.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/PWM.h>
#include <ti/display/Display.h>
/* Board Header file */
#include "Board.h"
/* Local Includes */
#include "simple_application_processor.h"
#include "Profile/simple_gatt_profile.h"
#include "Board.h"
#include "hall.h"
/*Definition of Stack size*/
#define TASK_STACK_SIZE      1024

/*private functions*/
static void *position_taskFxn(void *arg0);
void update_Position(void);

/*global variables*/
extern  uint8_t x;
extern  uint8_t y;
extern  uint8_t x_old;
extern  uint8_t y_old;
extern  int angle;
extern Display_Handle displayOut;

/*helper variable for length calculation*/
uint8_t l_old=0;

/*Creation of the position task*/
void position_createTask(void)
{
    pthread_t           thread;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    pthread_attr_init(&attrs);
    priParam.sched_priority = 2;

    detachState = PTHREAD_CREATE_DETACHED;
    retc = pthread_attr_setdetachstate(&attrs, detachState);

    if (retc != 0)
    {
        while(1);
    }

    pthread_attr_setschedparam(&attrs, &priParam);

    retc |= pthread_attr_setstacksize(&attrs, TASK_STACK_SIZE);

    if (retc != 0)
    {
        while(1);
    }

    /* Creating the task */
    retc = pthread_create(&thread, &attrs, position_taskFxn, NULL);

    if (retc != 0)
    {
        while(1);
    }
}
/*Position task
 * This task starts the application on position 128,128
 * In every loop the current Position is updated and the current length from the last update is calculated
 * */
static void *position_taskFxn(void *arg0)
{
        x=128;
        x_old=128;
        y=128;
        y_old=128;
 while(1){
        update_Position();
        SimpleProfile_SetParameter(SP_CHAR1_ID, sizeof(x), &x);
        SimpleProfile_SetParameter(SP_CHAR2_ID, sizeof(y), &y);
        Display_print1(displayOut, 0, 0,"Position x-value updated %i",x);
        Display_print1(displayOut, 0, 0,"Position y-values updated %i",y);
        usleep(1000000);
        }
 }

/*This is a helper function that updates to x and y Data to the current position data*/
void update_Position(){
    uint8_t l_now=get_length();
    uint8_t l=l_now-l_old;
    Display_print1(displayOut, 0, 0,"Current Angle: %d",angle);
    Display_print1(displayOut, 0, 0,"Current Length: %i",l);
    float x_help;
    float y_help;
    x_help=x_old+(uint8_t)round(l*sin(angle*0.0174533));
    y_help=y_old+(uint8_t)round(l*cos(angle*0.0174533));
    x=x_help;
    y=y_help;
    Display_print1(displayOut, 0, 0,"x_help: %f",x_help);
    Display_print1(displayOut, 0, 0,"y_help: %f",y_help);
    Display_print1(displayOut, 0, 0,"x: %i",x);
    Display_print1(displayOut, 0, 0,"y: %i",y);
    l_old=l_now;
    x_old=x;
    y_old=y;
}
