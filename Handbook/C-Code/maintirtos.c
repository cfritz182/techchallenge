/* External Defines */
#include <position.h>
#include <stdint.h>
#include <ti/sysbios/BIOS.h>
#include <ti/display/Display.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/Timer.h>
#include <ti/drivers/UART.h>

/* Standard Defines */
#include "Board.h"
#include "simple_application_processor.h"
#include "position.h"
#include "hall.h"
#include "adc.h"
#include "pwm.h"
/* Output display handle that will be used to print out all debug/log
 * statements
 */
Display_Handle displayOut;
/*GLOBAL VARIABLES*/

uint_fast16_t STATEFLAG=0;
int angle=0;
uint8_t x=0;
uint8_t y=0;
uint8_t x_old=0;
uint8_t y_old=0;
uint8_t length=0;
int hall_r_1_counter=0;
int hall_r_2_counter=0;
int hall_l_1_counter=0;
int hall_l_2_counter=0;
float length_help=0;

/*MAIN TI-RTOS FUNCTION*/
int main(void)
{
    /* Call board initialization functions */
    Power_init();
    GPIO_init();
    UART_init();
    Timer_init();

    /* Open the display for output */
    displayOut = Display_open(Display_Type_HOST | Display_Type_UART, NULL);
    if (displayOut == NULL)
    {
        /* Failed to open display driver */
        while (1);
    }

    /* Create main application processor tasks */
    //BLE TASK
    AP_createTask();
    //PWM TASK
    pwm_createTask();
    //HALL SENSOR TASK
    hall_createTask();
    //ADC TASK
    adc_createTask();
    //SEND POSITION DATA
    position_createTask();
    /* enable interrupts and start SYS/BIOS */
    BIOS_start();
    
    return 0;
}
