/* EDUROBOTICA 2018
 * This file reads in the Hall Sensor inputs. It can be done via threads to check
 * or via a GPIO interrupt (hwi-Interrupt). It measures the length of the current path
 *  hall.c
 */

/*Standart Includes*/
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/display/Display.h>
/* Board Header file */
#include "Board.h"

/* Extern variables*/
extern uint8_t length;
extern int angle;
extern Display_Handle displayOut;

/* Privat variables*/
/*Counter variables of the hall sensors*/
extern int hall_r_1_counter;
extern int hall_r_2_counter;
extern int hall_l_1_counter;
extern int hall_l_2_counter;

/*Helper Variable for length calculation*/
extern float length_help;

/*Definition of STACK SIZE*/
#define TASK_STACK_SIZE      2048

/*privat functions*/
void update_length();
int find_max();
void *hallThread(void *arg0);

/*Helper Function to find the maximum of the 4 Hall-Sensors*/
int find_max(){
  int max_r=0;
  int max_l=0;
  max_r=(hall_r_1_counter>hall_r_2_counter)?hall_r_1_counter:hall_r_2_counter;
  max_l=(hall_l_1_counter>hall_l_2_counter)?hall_l_1_counter:hall_l_2_counter;
  return (max_r>max_l)?max_r:max_l;
}

/*Updates to length of the path
 * This is be done by finding the maximum of the hallsensors and divide it
 * to get the path length in cm
 */
void update_length(){
  length_help=(float)find_max()/(7000.0)*22; //in cm
  length=(uint8_t)length_help;
  return;
}

/*Create Hall Sensor Task*/
void hall_createTask(void)
{
    pthread_t           thread;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    pthread_attr_init(&attrs);
    priParam.sched_priority = 1;

    detachState = PTHREAD_CREATE_DETACHED;
    retc = pthread_attr_setdetachstate(&attrs, detachState);

    if (retc != 0)
    {
        while(1);
    }

    pthread_attr_setschedparam(&attrs, &priParam);

    retc |= pthread_attr_setstacksize(&attrs, TASK_STACK_SIZE);

    if (retc != 0)
    {
        while(1);
    }

    /* Creating the task */
    retc = pthread_create(&thread, &attrs, hallThread, NULL);

    if (retc != 0)
    {
        while(1);
    }
}

/*Hall sensor Task
 * In this application we are using a simple task to check if the Pins are high or low
 */
void *hallThread(void *arg0)
{
  int counter_t=0;

  while (1) {

    if(GPIO_read(Board_GPIO_P2_3)==1){
        hall_l_2_counter++;
    }
    if(GPIO_read(Board_GPIO_P5_1)==1){
            hall_l_1_counter++;
    }
    if(GPIO_read(Board_GPIO_P3_5)==1){
            hall_r_1_counter++;
        }
    if(GPIO_read(Board_GPIO_P3_7)==1){
            hall_r_2_counter++;
        }
    if(counter_t>=1000){
        update_length();
        counter_t=0;
    }
    counter_t++;
     //  Display_print1(displayOut, 0, 0,"L A: %i",hall_l_1_counter);
     // Display_print1(displayOut, 0, 0,"L B: %i",hall_l_2_counter);
     //Display_print1(displayOut, 0, 0,"R A: %i",hall_r_1_counter);
     //Display_print1(displayOut, 0, 0,"R B: %i",hall_r_2_counter);
    usleep(1000);
  }

}
/*Getter function that returns length*/
uint8_t get_length(){
    return length;
}
/*Setter function which sets length to the input variable value*/
void set_length(uint8_t value){
    length=value;
    return;

}
