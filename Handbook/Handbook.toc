\contentsline {section}{Literatur}{1}{Doc-Start}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Package information}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}MSP-EXP432P401R SimpleLink™ MSP432P401R LaunchPad}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}BOOSTXL-CC2650MA BoosterPack}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}BEAGLEBK BeagleBone Black Development Board}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Pololu Romi Chassi}{7}{section.2.4}
\contentsline {section}{\numberline {2.5}Joystick}{9}{section.2.5}
\contentsline {chapter}{\numberline {3}Theory}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Pulse-Width-Modulation}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Digital Analog Converter}{11}{section.3.2}
\contentsline {section}{\numberline {3.3}GPIO interrupts}{12}{section.3.3}
\contentsline {section}{\numberline {3.4}Bluetooth Low Energy}{12}{section.3.4}
\contentsline {section}{\numberline {3.5}Real Time Operating Systems - RTOS}{12}{section.3.5}
\contentsline {chapter}{\numberline {4}Assembly of the Robot}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Kits}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}Partlist}{17}{section.4.2}
\contentsline {section}{\numberline {4.3}Assembly of the main board}{18}{section.4.3}
\contentsline {chapter}{\numberline {5}}{26}{chapter.5}
\contentsline {section}{\numberline {5.1}The main of the TI-RTOS}{26}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}main tirtos}{26}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}adc}{28}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}pwm}{31}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}position}{34}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}hall}{37}{subsection.5.1.5}
\contentsline {section}{\numberline {5.2}Board Specific files and Configurations}{40}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Board}{40}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}MSP EXP432P401R header file}{44}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}MSP EXP432P401R c file}{48}{subsection.5.2.3}
\contentsline {chapter}{\numberline {6}Bluetooth-Network}{65}{chapter.6}
\contentsline {section}{\numberline {6.1}Goal}{65}{section.6.1}
\contentsline {section}{\numberline {6.2}Setting up the Beaglebone}{66}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Flashing the latest firmware}{66}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Accessing the Beaglebone via ssh}{66}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Basic Terminal commands}{66}{subsection.6.2.3}
\contentsline {subsubsection}{cd,ls}{66}{section*.2}
\contentsline {subsubsection}{sudo}{66}{section*.3}
\contentsline {subsection}{\numberline {6.2.4}Internet connection with the Beaglebone}{67}{subsection.6.2.4}
\contentsline {subsection}{\numberline {6.2.5}Downloading and installing required packages}{68}{subsection.6.2.5}
\contentsline {section}{\numberline {6.3}Bluetooth Experiments}{69}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Check functionality with hci- and gatttool}{69}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}python-scripting}{69}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Writing the python code}{69}{section.6.4}
\contentsline {section}{\numberline {6.5}Visualising the data}{69}{section.6.5}
\contentsline {chapter}{\numberline {7}Troubleshooting}{70}{chapter.7}
\contentsline {section}{\numberline {7.1}ssh doesn't work}{70}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}ssh installed?}{70}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Cable correctly connected?}{70}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}no dhcp connection?}{70}{subsection.7.1.3}
\contentsline {section}{\numberline {7.2}Beaglebone: no LAN-connection possible}{71}{section.7.2}
\contentsline {section}{\numberline {7.3}sudo won't let me execute commands}{71}{section.7.3}
\contentsline {section}{\numberline {7.4}git clone throws an error}{72}{section.7.4}
\contentsline {chapter}{\numberline {8}Further Readings}{73}{chapter.8}
\contentsline {chapter}{\numberline {9}Building instructions for the cupboard}{74}{chapter.9}
