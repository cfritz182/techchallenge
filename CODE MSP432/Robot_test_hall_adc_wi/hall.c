/* EDUROBOTICA 2018
 * This file reads in the Hall Sensor inputs. It can be done via threads to check
 * or via a GPIO interrupt (hwi-Interrupt). It measures the length of the current path
 *  hall.c
 */

/*Standart Includes*/
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/display/Display.h>
/* Board Header file */
#include "Board.h"

/* Extern variables*/
extern float length;
extern int angle;
extern Display_Handle displayOut;

/* Privat variables*/
/*Counter variables of the hall sensors*/
extern int hall_l_2_counter;

/*Definition of STACK SIZE*/
#define TASK_STACK_SIZE      2048

/*privat functions*/
void update_length();
int find_max();
void *hallThread(void *arg0);

/*Updates to length of the path
 * This is be done by finding the maximum of the hallsensors and divide it
 * to get the path length in cm
 */
void update_length(){
  length+=(float)hall_l_2_counter/(700000.0)*22; //in cm
  return;
}

/*Create Hall Sensor Task*/
void hall_createTask(void)
{
    pthread_t           thread;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    pthread_attr_init(&attrs);
    priParam.sched_priority = 2;

    detachState = PTHREAD_CREATE_DETACHED;
    retc = pthread_attr_setdetachstate(&attrs, detachState);

    if (retc != 0)
    {
        while(1);
    }

    pthread_attr_setschedparam(&attrs, &priParam);

    retc |= pthread_attr_setstacksize(&attrs, TASK_STACK_SIZE);

    if (retc != 0)
    {
        while(1);
    }

    /* Creating the task */
    retc = pthread_create(&thread, &attrs, hallThread, NULL);

    if (retc != 0)
    {
        while(1);
    }
}

/*Hall sensor Task
 * In this application we are using a simple task to check if the Pins are high or low
 */
void *hallThread(void *arg0)
{
  bool new_status=0;
  bool old_status=0;
  while (1) {
      new_status=GPIO_read(Board_GPIO_P2_3);
    if(old_status==0 || new_status==1){
        hall_l_2_counter++;
        update_length();
    }
    old_status=new_status;
    usleep(1);
  }
}
