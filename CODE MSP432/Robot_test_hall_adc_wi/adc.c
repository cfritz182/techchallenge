/* EDUROBOTICA 2018
 * This file converts the Analog Data of the Joystick to Digital Values
 *  and sets the STATEFLAG if a collusion occures
 *  adc.c
 */

/*Standart Includes*/
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/ADC.h>
#include <ti/display/Display.h>

/* Board Header files */
#include "Board.h"

/* ADC sample count */
#define ADC_SAMPLE_COUNT  (10)

#define THREADSTACKSIZE   (768)

/* ADC conversion result variables
 * two variables for x and y - in Version 0.1 we are just using y-Value*/
uint16_t adcValue_x;
uint32_t adcValue_x_MicroVolt;
uint16_t adcValue_y;
uint32_t adcValue_y_MicroVolt;

/*Used to DEBUG*/
extern Display_Handle displayOut;
/*Used to send an collision to the pwm thread*/
extern uint_fast16_t STATEFLAG;

void *adc(void *arg0);

/* Creates the adc task
 * input: void
 * output: void*/
void *adc_createTask(void *arg0)
{
    /*All parameters wich are used to create a posix thread*/
    pthread_t           thread;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    /* Call driver init functions */
    ADC_init();

    /* Create application threads */
    pthread_attr_init(&attrs);
    priParam.sched_priority = 1;

    detachState = PTHREAD_CREATE_DETACHED;
            /* Set priority and stack size attributes */
            retc = pthread_attr_setdetachstate(&attrs, detachState);
            if (retc != 0) {
                /* pthread_attr_setdetachstate() failed */
                while (1);
            }
    pthread_attr_setschedparam(&attrs, &priParam);
    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0) {
        /* pthread_attr_setstacksize() failed */
        while (1);
    }

    /* Create adc thread */
    retc = pthread_create(&thread, &attrs, adc, NULL);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1);
    }

    return (NULL);
}

/*
 *  adc
 *  This thread reads in the sensor data from the joystick and checks if there is a collusion
 *  if so, the thread will set the STATEFLAG to 1
 */
void *adc(void *arg0)
{
       /*ADC Parameters for initialization*/
      ADC_Handle   adc;
      ADC_Params   params;
      int_fast16_t res;
      uint32_t adc_Value_mean=0;
      int first=0;

      /*Initalization of ADC_1 on the Board this is Pin 5.4*/
      ADC_Params_init(&params);
      adc = ADC_open(Board_ADC1, &params);
      /* Check of the initialization*/
      if (adc == NULL) {
          Display_printf(displayOut, 0, 0, "Error initializing ADC channel 1\n");
          while (1);
      }
      while(1){
      /* Blocking mode conversion */
      res = ADC_convert(adc, &adcValue_y);

      if (res == ADC_STATUS_SUCCESS) {

          adcValue_y_MicroVolt = ADC_convertRawToMicroVolts(adc, adcValue_y);
          /* Uncomment the line below to see ADC readings*/
          // Display_printf(displayOut, 0, 0, "ADC channel 1 convert result: %d uV", adcValue_y_MicroVolt);
      }
      else {
          /*Wrong convertion signaling*/
          Display_printf(displayOut, 0, 0, "ADC channel 1 convert failed");
      }
      /*If it is the first time the adc_mean value will be set to set it to the right level,
       * if batteries are low this must be done to overcome voltage drop issues
       */
      if(first==0){
                    first++;
                    adc_Value_mean=adcValue_y_MicroVolt;
                }

      /*If measured data is 1V below the averadge, set STATEFLAG to 1*/
      if(adcValue_y_MicroVolt<adc_Value_mean-1000000){
          STATEFLAG=1;
      }
      else{
          STATEFLAG=0;
      }
      /*Taks sleep to enable other tasks to run*/
      usleep(100000);
      }
  }
