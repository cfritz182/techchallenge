/* EDUROBOTICA 2018
 * This file controls the PWM signal to the motors.
 * It used the turn function if the STATEFLAG is 1, which means a collusion occured
 * The task also changes to current angle to the new one
 *  pwm.c
 */
/*Standart Includes*/
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

/* POSIX Header files */
#include <pthread.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/PWM.h>

/* Board Header file */
#include "Board.h"

/*global variables*/
extern uint_fast16_t STATEFLAG;
extern int angle;

/*Privat Variables*/
uint16_t   pwmPeriod = 3200;
PWM_Handle pwm1;
PWM_Handle pwm2;
PWM_Params params;

/*Privat Functions*/
void turn();
void set_Stop();
void set_Reverse();
void set_Forward();
void update_angle();
void *pwmThread(void *arg0);

#define TASK_STACK_SIZE      2048

/* This function Stops the Robot, sets Reverse for some time and than turns
 * for 70 degrees */

void turn(){
  int i;
  set_Stop();
  set_Reverse(6);
  set_Reverse(7);
  PWM_setDuty(pwm1,PWM_DUTY_FRACTION_MAX/4);
  PWM_setDuty(pwm2,PWM_DUTY_FRACTION_MAX/4);
  for(i=0; i<5000000;i++){
   }
  set_Stop();
  for(i=0; i<500000;i++){
   }
  PWM_setDuty(pwm2,PWM_DUTY_FRACTION_MAX/6);

  for(i=0; i<5000000;i++){
   }
  set_Forward(6);
  set_Forward(7);
  update_angle();
 }
/* Adds 70 degrees to current angle*/
void update_angle(){
  angle=((angle+70)>360)?(angle-290):(angle+70);
}
/* Stop the Motor*/
void set_Stop(){
  PWM_setDuty(pwm1,0);
  PWM_setDuty(pwm2,0);
}

/* Sets Pins LOW for Reverse*/
void set_Reverse(int j){
  if(j==6){
  GPIO_write(Board_GPIO_P2_6, 0);
   }
  else{
  GPIO_write(Board_GPIO_P2_7, 0);
   }
 }

/* Sets Pins HIGH for Reverse*/
void set_Forward(int j){
 if(j==6){
 GPIO_write(Board_GPIO_P2_6, 1);
  }
 else{
 GPIO_write(Board_GPIO_P2_7, 1);
  }
 }

/* PWM Thread creation*/
void pwm_createTask(void)
{
    pthread_t           thread;
    pthread_attr_t      attrs;
    struct sched_param  priParam;
    int                 retc;
    int                 detachState;

    pthread_attr_init(&attrs);
    priParam.sched_priority = 1;

    detachState = PTHREAD_CREATE_DETACHED;
    retc = pthread_attr_setdetachstate(&attrs, detachState);

    if (retc != 0)
    {
        while(1);
    }

    pthread_attr_setschedparam(&attrs, &priParam);

    retc |= pthread_attr_setstacksize(&attrs, TASK_STACK_SIZE);

    if (retc != 0)
    {
        while(1);
    }

    /* Creating the task */
    retc = pthread_create(&thread, &attrs, pwmThread, NULL);

    if (retc != 0)
    {
        while(1);
    }
}
/*This thread is used to control the PWM signal to the motors,
 * it switches to turn() if the STATEFLAG is 1
 */
void *pwmThread(void *arg0)
{ /*general initialization*/
  PWM_init();
  /*Setting of parameters for the PWM thread*/
  PWM_Params_init(&params);
  params.dutyUnits = PWM_DUTY_FRACTION;
  params.dutyValue = 0;
  params.periodUnits = PWM_PERIOD_US;
  params.periodValue = pwmPeriod;

  /*opens and starts PWM0*/
  pwm1 = PWM_open(Board_PWM0, &params);
  if (pwm1 == NULL) {
  /* Board_PWM0 did not open */
    while (1);
   }
  PWM_start(pwm1);

  // open & start pwm 2
  pwm2 = PWM_open(Board_PWM1, &params);
  if (pwm2 == NULL) {
  /* Board_PWM1 did not open */
    while (1);
  }
  PWM_start(pwm2);
  set_Forward(6);
  set_Forward(7);

  while (1) {
    if(STATEFLAG==1){
      STATEFLAG=0;
      turn();
     }
    else{
       /*Sets the PWM Duty length to 1/3 of the Maximum*/
      PWM_setDuty(pwm1,PWM_DUTY_FRACTION_MAX/3);
      PWM_setDuty(pwm2,PWM_DUTY_FRACTION_MAX/3);
      /*Sets sleep to the task*/
      usleep(10000);
     }
  }
}
