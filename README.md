# Welcome to the Techchallenge 2017
#   EDUROBOTICA
This is our repository for developing our code and our business plan  
Project Name: EvoSense

## Useful links:
* [Slack Channel](https://utum-techchallenge.slack.com/home) for getting all infos
* [CCS Studio](http://www.ti.com/tool/CCSTUDIO) link to CCS Studio
* [TI Board](http://www.ti.com/tool/MSP-EXP432P401R) our development board
* [BoosterPacks](http://www.ti.com/tools-software/launchpads/boosterpacks/build.html) list of available Boster-Packs
* [Pololu Chassis](https://www.pololu.com/product/3543) the chassis we use
## Open tasks:
- [x] Find a project
- [x] Project name?
- [x] Create student survey about room conditions
- [x] Ask for relevant data at different environmental engineering chairs
- [x] Start coding
- [x] Start writing business plan
- [x] Printing of chassi